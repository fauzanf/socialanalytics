import VuetifyLoaderPlugin from 'vuetify-loader/lib/plugin'
import { fireDb, fireStore } from './plugins/firebaseInit.js'
import pkg from './package'
require('dotenv').config();


export default {
  mode: 'spa',

  /*
  ** Environtment Variables
  */
  env: {
    baseUrl: process.env.BASE_URL,
    base: process.env.base,
    title: process.env.title,
    desc: process.env.desc,
    logo: process.env.logo,
    port: process.env.PORT,
    googleId: process.env.GOOGLE_ANALYTICS_ID,
    urlIG: process.env.urlIG,
    clientId: process.env.clientId,
    clientSecret: process.env.clientSecret,
    igClientId: process.env.igClientId,
    igClientSecret: process.env.igClientSecret,
    redirectUri: process.env.redirect_uri,
    urlIgGraph: process.env.urlIgGraph,
  },

  hooks: {
    generate: {
      done(builder) {
        fireDb.goOffline()
      }
    }
  },

  firebase: {
    config: {
      apiKey: "AIzaSyByEVYrjnnQNdspH_qDBBQc53K4fjL2e1o",
      authDomain: "cermat-jarak-jauh.firebaseapp.com",
      databaseURL: "https://cermat-jarak-jauh.firebaseio.com",
      projectId: "cermat-jarak-jauh",
      storageBucket: "cermat-jarak-jauh.appspot.com",
      messagingSenderId: "126553074757",
      appId: "1:126553074757:web:07481daefe6b7b44e4872c",
      measurementId: "G-1SMPQLH1Z7"
    },
    services: {
      auth: true // Just as example. Can be any other service.
    }
  },

  googleAnalytics: {
    // Options
    id: process.env.GOOGLE_ANALYTICS_ID,
    autoTracking: {
      screenview: true
    },
    dev: true,
    debug: {
      enabled: true
    }
  },

  publicRuntimeConfig: {
    googleAnalytics: {
      id: process.env.GOOGLE_ANALYTICS_ID,
      autoTracking: {
        screenview: true
      },
      dev: true
    }
  },

  auth: {
    redirect: {
      login: '/login',
      logout: '/',
      callback: '/login',
      user: '/',
      home: '/'
    },
    strategies: {
      facebook: {
        endpoints: {
          userInfo: 'https://graph.facebook.com/me?fields=id,name,picture{url}'
        },
        clientId: process.env.clientId,
        scope: ['public_profile', 'email']
      },
      local1: {
        _scheme: 'local',
        endpoints: {
          login: {
            url: process.env.base + '/auth/employee/login',
            method: 'post',
            propertyName: 'token'
          },
          logout: false,
          user: {
            url: process.env.base + '/employee/profile',
            method: 'get',
            propertyName: ''
          }
        },
        tokenRequired: true,
        tokenType: 'bearer',
      },
      local2: {
        _scheme: 'local',
        endpoints: {
          login: {
            url: process.env.base + '/auth/admin/login',
            method: 'post',
            propertyName: 'token'
          },
          logout: false,
          // user: {
          //   url: '',
          //   method: 'get',
          //   propertyName: 'data'
          // }
        },
        tokenRequired: true,
        tokenType: 'bearer',
      }
    }
  },

  /*
  ** Headers of the page
  */
  head: {
    title: 'Social-Analytics',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: pkg.description }
    ],
    script: [
      {
        src: 'https://polyfill.io/v3/polyfill.min.js?features=es6'
      },
      {
        src: 'https://cdn.jsdelivr.net/npm/mathjax@3/es5/tex-mml-chtml.js'
      },
      {
        src: 'https://cdnjs.cloudflare.com/ajax/libs/mathjax/2.7.4/MathJax.js?config=TeX-MML-AM_CHTML'
      },
      {
        src: 'https://cdn.mathjax.org/mathjax/latest/MathJax.js?config=TeX-AMS-MML_HTMLorMML'
      },{
        src: '/js/fb-sdk.js'
      }
    ],
    link: [
      // /cermat_favicon.jpeg
      { rel: 'icon', type: 'image/x-icon', href: '/logokemendikbud.png' },
      {
        rel: 'stylesheet',
        href:
          'https://fonts.googleapis.com/css?family=Libre+Franklin:300,400,500,700,800,900&display=swap'
      },
      {
        rel: 'stylesheet',
        href:
          'https://fonts.googleapis.com/css?family=PT+Sans&display=swap'
      },
      {
        rel: 'stylesheet',
        href:
          'https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Material+Icons'
      },
      {
        rel: 'mathjax',
        href: ''
      }
    ]
  },

  /* nuxt.js Router */
  router: {
    middleware: ['auth']
  },

  /*
  ** Customize the progress-bar color
  */
  loading: { color: '#fff' },

  /*
  ** Global CSS
  */
  css: [
    '~/assets/style/app.styl',
    '~/static/main.css',
    // 'https://unpkg.com/mathlive/dist/mathlive-static.css'
  ],

  /*
  ** Plugins to load before mounting the App
  */
  plugins: [
    '@/plugins/vuetify',
    '@/plugins/auth',
    '@/plugins/validate',
    '@/plugins/axios',
    '@plugins/xlsx',
    '@/plugins/google_analytic',
    '@/plugins/aos.client',
    '@/plugins/vuecrontab',
    { src: "~plugins/quill.js", ssr: false },

    //Models
    '~/models/user',
    '~/models/region',
    '~/models/lesson',
    '~/models/question',
    '~/models/institution',
    '~/models/rombel',
    '~/models/quiz',
    '~/models/result',
    '~/models/statistic',
    '~/models/domain',
    '~/models/admin',
    '~/models/authig',
    '~/models/topchart',
    '~/models/hashtag',
    '~/models/workunit',
  ],

  /*
  ** Nuxt.js modules
  */
//  buildModules: [
//     // Doc: https://github.com/nuxt-community/eslint-module
//     '@nuxtjs/eslint-module'
//   ],
  modules: [
    // Doc: https://axios.nuxtjs.org/usage
    '@nuxtjs/axios',
    '@nuxtjs/pwa',
    '@nuxtjs/auth',
    '@nuxtjs/dotenv',
    '@nuxtjs/firebase',
    '@nuxtjs/google-analytics',
  ],
  /*
  ** Axios module configuration
  */
  axios: {
    // See https://github.com/nuxt-community/axios-module#options
  },

  /*
  ** Build configuration
  */
  build: {
    transpile: ['vuetify/lib'],
    modules: [],
    plugins: [new VuetifyLoaderPlugin()],
    loaders: {
      stylus: {
        import: ['~assets/style/variables.styl']
      }
    },
    /*
    ** You can extend webpack config here
    */
    // extend(config, ctx) {
    //   config.resolve.alias["vue"] = "vue/dist/vue.common";
    //   config.module.rules.forEach( rule => {
    //     if ( isCssRule(rule) ) {
    //       rule.exclude = /ckeditor5-[^/]+\/theme\/[\w-/]+\.css$/;
    //     }
    //   });

    //   config.module.rules.unshift(
    //     {
    //       // Or /ckeditor5-[^/]+\/theme\/icons\/[^/]+\.svg$/ if you want to limit this loader
    //       // to CKEditor 5 icons only.
    //       test: /ckeditor5-[^/]+\/theme\/icons\/[^/]+\.svg$/,
    //       use: ['raw-loader']
    //     },
    //     {
    //       // Or /ckeditor5-[^/]+\/theme\/[\w-/]+\.css$/ if you want to limit this loader
    //       // to CKEditor 5 theme only.
    //       test: /ckeditor5-[^/]+\/theme\/[\w-/]+\.css$/,
    //       use: [
    //         {
    //           loader: 'style-loader',
    //           options: {
    //             injectType: "singletonStyleTag"
    //           }
    //         },
    //         {
    //           loader: 'postcss-loader',
    //           options: styles.getPostCssConfig({
    //             themeImporter: {
    //               themePath: require.resolve('@ckeditor/ckeditor5-theme-lark')
    //             },
    //             minify: true
    //           })
    //         },
    //       ]
    //     }
    //   );

    //   // CKEditor needs its own plugin to be built using webpack.
    //   config.plugins.unshift(
    //     new CKEditorWebpackPlugin({
    //       // See https://ckeditor.com/docs/ckeditor5/latest/features/ui-language.html
    //       language: 'en'
    //     })
    //   );
    // }
  },

}
function isCssRule(rule) {
  return rule.test.toString().indexOf('css') > -1;
}
