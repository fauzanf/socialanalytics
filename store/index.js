export const state = () => ({
  sidebar: false,
  country: 'Indonesia',
  
  roles: {
    isAdminPusat: 'pusat',
    isAdminProvinsi: 'provinsi',
    isAdminKokab: 'kokab',
    isAdminKKG: 'kkg',
    isAdminSekolah: 'sekolah',
    isSuperAdmin: 'superadmin'
    // isGuru: 'guru',
    // isMurid: 'murid',
  },

  types: [
    // { text: 'Pusat', value: 'pusat' },
    { text: 'Provinsi', value: 'provinsi' },
    { text: 'Kota/Kabupaten', value: 'kokab' },
    { text: 'Kelompok Kerja Guru', value: 'kkg' },
    { text: 'Sekolah', value: 'sekolah' },
    { text: 'Guru', value: 'guru' },
  ],

  dialog: {
    display: false,
    class: '',
    title: '',
    message: '',
    target: '/',
    reload: false
  },

  preview: {
    status: false,
    data: [],
    selected: 0,
    custom_matrix: []
  },

  subjects: [
    { text: 'Matematika', value: 'matematika' },
    { text: 'Bahasa Indonesia', value: 'bahasa-indonesia' },
    { text: 'Bahasa Inggris', value: 'bahasa-inggris' },
  ],
  
  lessons: [ 'Matematika', 'Bahasa Indonesia', 'Bahasa Inggris'],

  level: ['SD', 'SMP', 'SMA'],

  grades: ["1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12"],

  difficulties: [
    "Sangat Mudah",
    "Mudah",
    "Sedang / Netral",
    "Sukar",
    "Sangat Sukar",
  ],

  semesters: [ 1, 2 ]
})

export const mutations = {
  toggleSidebar(state) {
    state.sidebar = !state.sidebar
  },

  openDialog(state, data) {
    state.dialog.display = true
    state.dialog.class = data.class
    state.dialog.title = data.title
    state.dialog.message = data.message
    state.dialog.target = data.target
    state.dialog.reload = data.reload ? true : false
  },

  closeDialog(state) {
    state.dialog.display = false
    state.dialog.class = ''
    state.dialog.title = ''
    state.dialog.message = ''
    state.dialog.target = '/'
    state.dialog.reload = false
  },

  pratinjau(state, data) {
    state.preview.selected = 0
    state.preview.status = data.status
    state.preview.data[0] = data.data
    console.log(state.preview.data);

    var new_col = []
    if (data.data.column) {
      for (const key in data.data.column) {
        if (data.data.column.hasOwnProperty(key)) {
          const element = data.data.column[key];
          var col_str = JSON.stringify(element)
          col_str = col_str.replace(/"<p>/gi, '')
          col_str = col_str.replace(/<\/p>"/gi, '')
          col_str = col_str.replace(/\\/gi, '')
          if (col_str.includes("#")) {
            var temp_col = col_str.split('#')
            console.log('ttc', temp_col);
            new_col.push(temp_col)
          } else {
            console.log('nada');
          }
        }
      }
    }

    state.preview.custom_matrix[0] = new_col

    console.log(new_col);
  },

  pratinjauKuis(state, data) {
    state.preview.selected = 0
    state.preview.status = data.status
    state.preview.data = data.data
    // console.log(state.preview.data);

    var new_col = {}
    for (const x in data.data) {
      new_col[x] = []
      if (data.data.hasOwnProperty(x)) {
        const data_column = data.data[x].column;
        // console.log(x, data_column)

        for (const key in data_column) {
          if (data_column.hasOwnProperty(key)) {
            const element = data_column[key];
            var col_str = JSON.stringify(element)
            if (col_str.includes("#")) {
              col_str = col_str.replace(/"<p>/gi, '')
              col_str = col_str.replace(/<\/p>"/gi, '')
              col_str = col_str.replace(/\\/gi, '')
              var temp_col = col_str.split('#')
              // console.log(x+' ttc', temp_col);
              new_col[x].push(temp_col)
            } else {
              // new_col[x].push(null)
              console.log('nada');
            }
          }
        }
      }
    }

    console.log('new col : ', new_col);
    state.preview.custom_matrix = new_col
  },

  next(state) {
    state.preview.selected = state.preview.selected + 1
  }, prev(state) {
    state.preview.selected = state.preview.selected - 1
  }
}