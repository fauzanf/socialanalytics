import { HorizontalBar } from 'vue-chartjs'


export default {
  extends: HorizontalBar,
  props: ['data', 'options', 'plugins'],
  mounted () {
    this.renderChart(this.data, this.options)
  }
}