import { Line } from 'vue-chartjs'

export default {
  extends: Line,
  props: ['chartdata', 'options', 'plugins'],
  mounted () {
    this.renderChart(this.chartdata, this.options)
  }
}