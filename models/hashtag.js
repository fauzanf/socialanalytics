const url = process.env.base
const model = $axios => ({

  addHashtag({
    hashtag
  }) {
    // return $axios.$post(url + '/public/hashtag', {
    //   hashtag
    // })
    return $axios.$post(url + '/admin/hashtag', {
      hashtag
    })
  },

  getHashtag({limit, page}) {
    let parameters = {
      "page": page,
      "limit": limit,
    }
    for (const key in parameters) {
        if (parameters.hasOwnProperty(key)) {
            const element = parameters[key];
            if (parameters[key] == '') {
                delete parameters[key]
            }
        }
    }
    return $axios.$get(url + '/public/hashtag', {
      params: parameters
    })
    // return $axios.$get(url + '/admin/hashtag', {
    //   params: parameters
    // })
  },

  updateHashtag({_id, data}) {
    // console.log(username);
    // console.log(data);
    
    // return $axios.$patch(url + '/public/hashtag', {
    //   _id,
    //   data
    // })
    return $axios.$patch(url + '/admin/hashtag', {
      _id,
      data
    })
  },

  deleteHashtag({_id}) {    
    // return $axios.$delete(url + '/public/hashtag', {
    //   data: { _id }      
    // })
    return $axios.$delete(url + '/admin/hashtag', {
      data: { _id }      
    })
  },

  addPost({id, user, urlMedia, type, commentCount, likeCount, shareCount, timestamp}) {
    return $axios.$post(url + '/admin/post', {
      id, user, "url": urlMedia, type, commentCount, likeCount, shareCount, timestamp
    })
  }
})

export default ({
  $axios
}, inject) => {
  inject('hashtag', model($axios));
}