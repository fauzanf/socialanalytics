const url = process.env.base
const model = $axios => ({

  addAdmin({
    phoneNumber,
    fullName
  }) {
    return $axios.$post(url + '/user/admin', {
      phoneNumber,
      fullName
    })
  },

  deleteAdmin({
    _id
  }) {
    return $axios.$delete(url + '/user/admin', {
      data: {_id}
    })
  },

  getAdmin() {
      return $axios.$get(url + '/user/admin')
  },

  updateAdmin({_id, data}) {
    return $axios.$put(url + '/user/admin', {
      _id,
      data
    })
  },
})

export default ({
  $axios
}, inject) => {
  inject('admin', model($axios));
}