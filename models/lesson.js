const url = process.env.base
const model = $axios => ({
  getLesson() {
    return $axios.$get(url + '/lesson')
  },
})

export default ({
  $axios
}, inject) => {
  inject('lesson', model($axios));
}