const url = process.env.base
const model = $axios => ({
  getStatQuiz() {
    return $axios.$get(url + '/statistic/quiz')
  },
  getStatMurid() {
    return $axios.$get(url + '/statistic/murid')
  },
  getStatResult() {
    return $axios.$get(url + '/statistic/result')
  },
  getStatSchool() {
    return $axios.$get(url + '/statistic/school')
  },
  getStatGuru() {
    return $axios.$get(url + '/statistic/guru')
  },
})

export default ({
  $axios
}, inject) => {
  inject('statistic', model($axios));
}