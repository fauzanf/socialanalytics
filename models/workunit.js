const url = process.env.base
const model = $axios => ({

  addWorkUnit({
    name
  }) {
    return $axios.$post(url + '/admin/workunit', {
      name
    })
  },

  getWorkUnit({limit, page}) {
    let parameters = {
      "page": page,
      "limit": limit,
    }
    for (const key in parameters) {
        if (parameters.hasOwnProperty(key)) {
            const element = parameters[key];
            if (parameters[key] == '') {
                delete parameters[key]
            }
        }
    }
    return $axios.$get(url + '/public/workunit', {
      params: parameters
    })
    
  },

  updateWorkUnit({_id, data}) {
    // console.log(username);
    // console.log(data);
    
    return $axios.$patch(url + '/admin/workunit', {
      _id,
      data
    })
  },

  deleteWorkUnit({_id}) {    
    return $axios.$delete(url + '/admin/workunit', {
      data: { _id }      
    })
  }
})

export default ({
  $axios
}, inject) => {
  inject('workunit', model($axios));
}