const url = process.env.base
const model = $axios => ({
  getProvinces() {
    return $axios.$get(url + '/region')
  },

  getCities({provinceId}) {
    return $axios.$get(url + '/region/cities/' + provinceId)
  },

  getDistricts({cityId}) {
    return $axios.$get(url + '/region/districts/' + cityId)
  },

  getVillages({districtId}) {
    return $axios.$get(url + '/region/villages/' + districtId)
  },

  getRegion() {
    return $axios.$get(url + '/region')
  }

})

export default ({
  $axios
}, inject) => {
  inject('region', model($axios));
}