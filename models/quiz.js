const url = process.env.base
const model = $axios => ({
  createQuiz({
    name, questions, department, lesson, grade, duration, type, startDateTime, endDateTime, allowedFor
  }) {
    return $axios.$post(url + '/quiz', {
      name, questions, department, lesson, grade, duration, type, startDateTime, endDateTime, allowedFor
    })
  },

  addQuiz({
    name, classes, questionSubject, duration, published, flowGroup, startDateTime, endDateTime, provinces, cities, subDistrict, rules, type
  }) {
    return $axios.$post(url + '/quiz', {
      name, duration, published, classes, flowGroup: flowGroup, rules, type, questionSubject,
      restriction: { 
        startDateTime, endDateTime, provinces, cities, subDistrict
      }
    })
  },

  editQuiz({
    _id, item
  }) {
    return $axios.$put(url + '/quiz', {
      _id, data: {
        name: item.name, published: item.published, classes: item.classes, flowGroup: item.flowGroup, rules: item.rules, type: item.type, questionSubject: item.questionSubject,
        restriction: { 
          startDateTime: item.restriction.startDateTime, endDateTime: item.restriction.endDateTime, provinces: item.restriction.provinces, cities: item.restriction.cities, subDistrict: item.restriction.subDistrict
        }
      }
    })
  },
  
  updateQuiz({
    id, data
  }) {
    // console.log(data);
    return $axios.$put(url + '/quiz', {
      id, data
    })
  },

  openQuiz({
    id
  }) {
    return $axios.$put(url + '/quiz', {
      id, data: {
        published: true
      }
    })
  },
  
  closeQuiz({
    id
  }) {
    return $axios.$put(url + '/quiz', {
      id, data: {
        published: false
      }
    })
  },

  getQuizes(startDateTime) {
    return $axios.$get(url + '/quiz', {
      params: startDateTime
    })
  },

  deleteQuiz({_id}) {    
    return $axios.$delete(url + '/quiz', {
      data: { _id }      
    })
  }
  
})

export default ({
  $axios
}, inject) => {
  inject('quiz', model($axios));
}