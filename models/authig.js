const url = process.env.urlIG
const urlIgGraph = process.env.urlIgGraph
const client_id = process.env.igClientId
const client_secret = process.env.igClientSecret
const redirect_uri = process.env.redirect_uri
const oauth = require('axios-oauth-client');
const model = $axios => ({
    getToken() {
        return $axios.$get(url + '/oauth/authorize', {
            params: {
                client_id,
                "redirect_uri": redirect_uri,
                "scope": "user_profile,user_media",
                "response_type": "code",
                "state": 1,
            }
        })
    },

    getAccessToken({code}) {
        let form = new FormData();
        form.append("client_id", client_id)
        form.append("client_secret", client_secret)
        form.append("redirect_uri", redirect_uri)
        form.append("code", code)
        form.append("grant_type", "authorization_code")
        return oauth.client($axios.create(), {
            url: url + "/oauth/access_token",
            grant_type: "authorization_code",
            code: code,
            client_id: client_id,
            client_secret: client_secret,
            redirect_uri: redirect_uri
        });
        // return $axios.$post(url + '/oauth/access_token', form)
    },

    getLiveToken({access_token}) {
        return $axios.$get(urlIgGraph + '/access_token', {
            params: {
                grant_type: "ig_exchange_token",
                client_secret,
                access_token
            }
        })
    },
    
    getProfileMedia({access_token}) {
        return $axios.$get(urlIgGraph + '/me', {
            params: {
                fields: "username,media_count,account_type,media.caption",
                access_token
            }
        })
    }
})

export default ({
    $axios
}, inject) => {
    inject('authIG', model($axios));
}