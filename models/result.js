const url = process.env.base
const model = $axios => ({
  getResults({ id }) {
    return $axios.$get(url + '/result?quizId=' + id + '&isAddQuestionDetail=true')
  },

  getStudentResults({ id, student_id }) {
    return $axios.$get(url + '/result', {
      params: {
        quiz: id, muridID: student_id
      }
    })
  },

  getByProvince({ id, province, city }) {
    return $axios.$get(url + '/result/location', {
      params: {
        quiz: id, province, city
      }
    })
  },

  deleteResult({_id}) {
    return $axios.$delete(url + '/result', {
      data: { _id }
    })
  }
})

export default ({
  $axios
}, inject) => {
  inject('result', model($axios));
}