const url = process.env.base
const model = $axios => ({
  getRombels({ school }) {
    return $axios.$get(url + '/rombel?school=' + school)
  },

  getUserRombel({id}) {
    return $axios.$get(url + '/user?rombel=' + id)
  },
  
})

export default ({
  $axios
}, inject) => {
  inject('rombel', model($axios));
}