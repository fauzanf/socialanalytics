const url = process.env.base
const model = $axios => ({
  createInstitution({
    name, type, province, city
  }) {
    if (type == 'provinsi') {
      return $axios.$post(url + '/institution', {
        name, type, province
      })
    } else {
        return $axios.$post(url + '/institution', {
          name, type, province, city
        })
    }
  },
  
  updateInstitution({
    id, name
  }) {
    return $axios.$put(url + '/institution', {
      id, data: {name}
    })
  },

  getInstitutions() {
    return $axios.$get(url + '/institution')
  },
  
  getSchool() {
    return $axios.$get(url + '/institution/school/claim')
  },

  cekNPSN({npsn}) {
    return $axios.$post(url + '/institution/npsn', { npsns: npsn })
  },

  createSchool({npsn}) {
    return $axios.$post(url + '/institution/school', {
      type: 'sekolah', 
      npsn: npsn 
    })
  },

  getUnclaimed() {
    return $axios.$get(url + '/institution/school/unclaimed')
  },

  claimSchool({institution}) {
    return $axios.$post(url + '/institution/school/claim', {
      id: institution
    })
  },
  
  unclaimSchool({institution}) {
    return $axios.$post(url + '/institution/school/unclaim', {
      id: institution
    })
  },

  deleteInstitution({id}) {    
    return $axios.$delete(url + '/institution', {
      data: { id }      
    })
  }
  
})

export default ({
  $axios
}, inject) => {
  inject('institution', model($axios));
}