const url = process.env.base
const model = $axios => ({

  addUser({
    name, twitterId, facebookId, instagramId, username, password, workUnitId, email
  }) {
    return $axios.$post(url + '/admin/employee', {
      name,
      username,
      password,
      workUnitId,
      twitterId,
      facebookId,
      instagramId,
      email,
      facebookToken: ''
    })
  },

  getUsers({limit, page}) {
    let parameters = {
      "page": page,
      "limit": limit,
    }
    for (const key in parameters) {
        if (parameters.hasOwnProperty(key)) {
            const element = parameters[key];
            if (parameters[key] == '') {
                delete parameters[key]
            }
        }
    }
    return $axios.$get(url + '/admin/employee', {
      params: parameters
    })
    
  },

  getUserById({userId}) {
    return $axios.$get(url + '/admin/employee/'+userId)
  },

  updateUser({_id, data}) {
    // console.log(username);
    // console.log(data);
    
    return $axios.$patch(url + '/admin/employee', {
      _id,
      data
    })
  },

  changePassword({username, new_pass}) {
    return $axios.$put(url + '/user', {
      username,
      data: {
        password: new_pass,
        default_password: new_pass
      }
    })
  },

  deleteUser({_id}) {    
    return $axios.$delete(url + '/admin/employee', {
      data: { _id }      
    })
  },

  getProfile() {
    return $axios.$get(url + '/employee/profile');
  },

  updateProfile({twitterId, facebookId, facebookToken, instagramId, instagramToken, name, email}) {
    return $axios.$patch(url + '/employee/profile', {
      twitterId, 
      facebookId, 
      facebookToken, instagramId, instagramToken, name, email
    })
  },

  register({name, workUnitId, username,password, twitterId, facebookId, facebookToken, instagramId, instagramToken, email}) {
    return $axios.$post(url + '/auth/employee/register', {
      name,
      workUnitId,
      username,
      password,
      twitterId,
      facebookId,
      facebookToken,
      instagramId,
      instagramToken,
      email
    })
  },

  sendEmail({_id, _ids, all}) {
    let parameters = {
      "_id": _id,
      "_ids": _ids,
      "all": all,
    }
    for (const key in parameters) {
        if (parameters.hasOwnProperty(key)) {
            const element = parameters[key];
            if (parameters[key] == '') {
                delete parameters[key]
            }
        }
    }

    return $axios.$get(url + "/admin/employee/email/send", {
      params: parameters
    })
  }
})

export default ({
  $axios
}, inject) => {
  inject('user', model($axios));
}