const url = process.env.base
const model = $axios => ({
    getDomain() {
        return $axios.$get(url + '/domain')
    },

    getSingleDomain({type}) {
        return $axios.$get(url + '/domain', {
            params: {
                type
            }
        })
    },

    addDomain({name, type, grade, subject, data}) {
        return $axios.$post(url + '/domain', {
            name,
            type,
            grade,
            subject,
            'subs': data
        });
    },

    updateDomain({_id, name, type, grade, subject, data}) {
        return $axios.$put(url + '/domain', {
            _id,
            data: {
                name,
                type,
                grade,
                subject,
                'subs': data
            }
            
        });
    },

    deleteDomain({_id}) {
        return $axios.$delete(url + '/domain', {
            data: {_id}
        });
    },

    addSubdomain({_id, subs}) {
        return $axios.$post(url + '/domain/sub', {
            _id,
            'subs': subs
        });
    }
})

export default ({
    $axios
  }, inject) => {
    inject('domain', model($axios));
  }