const url = process.env.base
const model = $axios => ({

    getTotalPost() {
        return $axios.$get(url + '/total/post')
    },

    getTotalPostDay({date}) {
        let parameters = {
            "date": date,
        }
        for (const key in parameters) {
            if (parameters.hasOwnProperty(key)) {
                const element = parameters[key];
                if (parameters[key] == '') {
                    delete parameters[key]
                }
            }
        }
        return $axios.$get(url + '/total/post/day', {
            params: parameters
        })
    },

    getTotalUser() {
        return $axios.$get(url + '/total/user')
    },

    getEmpTotalPostDay({date}) {
        let parameters = {
            "date": date,
        }
        for (const key in parameters) {
            if (parameters.hasOwnProperty(key)) {
                const element = parameters[key];
                if (parameters[key] == '') {
                    delete parameters[key]
                }
            }
        }
        return $axios.$get(url + '/employee/total/day', {
            params: parameters
        })
    },

    getTopUser({startAt, endAt}) {
      let parameters = {
          "startAt": startAt,
          "endAt": endAt,
      }
      for (const key in parameters) {
          if (parameters.hasOwnProperty(key)) {
              const element = parameters[key];
              if (parameters[key] == '') {
                  delete parameters[key]
              }
          }
      }
      return $axios.$get(url + '/total/user/social', {
          params: parameters
      })
    },

    getTopEnggagement({startAt, endAt}) {
      let parameters = {
          "startAt": startAt,
          "endAt": endAt,
      }
      for (const key in parameters) {
          if (parameters.hasOwnProperty(key)) {
              // const element = parameters[key];
              if (parameters[key] == '') {
                  delete parameters[key]
              }
          }
      }

      return $axios.$get(url + '/total/user/social', {
          params: parameters
      })
    },

    getTopEnggagementDay({date, limit}) {
      let parameters = {
          "date": date,
          "limit": limit,
      }
      for (const key in parameters) {
          if (parameters.hasOwnProperty(key)) {
              // const element = parameters[key];
              if (parameters[key] == '') {
                  delete parameters[key]
              }
          }
      }

      return $axios.$get(url + '/total/user/social/engagement/day', {
          params: parameters
      })
  },

    getEmpPost() {
        return $axios.$get(url + '/employee/statistic')
    },

    getEmpEnggagement({filter, limit}) {
        let parameters = {
            "filter": filter,
            "limit": limit,
        }
        for (const key in parameters) {
            if (parameters.hasOwnProperty(key)) {
                const element = parameters[key];
                if (parameters[key] == '') {
                    delete parameters[key]
                }
            }
        }

        return $axios.$get(url + '/employee/statistic/engagement', {
            params: parameters
        })
    },

    flushData() {
        return $axios.$get(url + '/admin/data/flush')
    },

    pullData(){
        return $axios.$get(url + '/admin/data/pull')
    }
})

export default ({
  $axios
}, inject) => {
  inject('topchart', model($axios));
}