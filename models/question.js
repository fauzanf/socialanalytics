const url = process.env.base
const model = $axios => ({
  getSIAPQuestions({
    kd, cakupan_materi, skki, kelas, jenjang, page, lesson,
    kode_soal
  }) {
    let parameters = {
      'kd.kode': '',
      'kd.description': '',
      'skki.kode': '',
      'skki.description': '',
      'cakupan_materi.kode': '',
      'cakupan_materi.description': '',
      'subkurikulum.kelas': kelas,
      'subkurikulum.jenjang': jenjang,
      page,
      'lesson': lesson,
      'kode_soal': ''
    }

    kd.kode ? parameters['kd.kode'] = kd.kode : delete parameters['kd.kode'];
    kd.description ? parameters['kd.description'] = kd.description : delete parameters['kd.description'];
    cakupan_materi.kode ? parameters['cakupan_materi.kode'] = cakupan_materi.kode : delete parameters['cakupan_materi.kode'];
    cakupan_materi.description ? parameters['cakupan_materi.description'] = cakupan_materi.description : delete parameters['cakupan_materi.description'];
    skki.kode ? parameters['skki.kode'] = skki.kode : delete parameters['skki.kode'];
    skki.description ? parameters['skki.description'] = skki.description : delete parameters['skki.description'];
    kode_soal ? parameters['kode_soal'] = kode_soal : delete parameters['kode_soal'];

    console.log(parameters);
    
    
    return $axios.$get(url+ '/question', {
      params: parameters
    })
  },

  getBankSoal({code, type, subject, grade, cognitiveDomain, cognitiveSubDomain, contentDomain, contentSubDomain, difficulty}) {
    let parameters = {
      "code.like": code,
      "type": type,
      "subject": subject,
      "grade": grade,
      "cognitiveDomain": cognitiveDomain,
      "cognitiveSubDomain": cognitiveSubDomain,
      "contentDomain": contentDomain,
      "contentSubDomain": contentSubDomain,
      "difficulty": difficulty
    }
    for (const key in parameters) {
      if (parameters.hasOwnProperty(key)) {
        const element = parameters[key];
        if (parameters[key] == '') {
          delete parameters[key]
        }
      }
    }
    
    return $axios.$get( url+ '/question', {
      params: parameters
      // {
      //   code, type, subject, cognitiveDomain, cognitiveSubDomain, contentDomain, contentSubDomain, difficulty
      // }
    })
  },

  getSIAPQuestionsById({
    kode_soal
  }) {
    return $axios.$get( url+ '/question', {
      params: {
        id: kode_soal
      }
    })
  },

  getQuestion({
    year,
    semester,
    subject,
    grade,
    difficulty,
    kd
  }) {
    return $axios.$get(url + '/question', {
      params: {
        'period.year': year,
        'period.semester': semester,
        subject,
        grade,
        difficulty,
        kd
      }
    })
  },

  createQuestion({soal, kd, cakupan_materi, subkurikulum, skki, lesson }) {
    return $axios.$post(url + '/question', {
      soal, kd, cakupan_materi, subkurikulum, skki,
      last_updated: new Date(), published: false, lesson
    })
  },

  addQuestion({code, type, subject, question, cognitiveDomain, cognitiveSubDomain, contentDomain, contentSubDomain, difficulty, answers, correctAnswers, grade}) {
    return $axios.$post(url + '/question', {
      code, type, subject, question, cognitiveDomain, cognitiveSubDomain, contentDomain, contentSubDomain, difficulty, grade,
      answers: answers,
      correctAnswers: correctAnswers
    })
  },

  editQuestion({_id, data}) {
    return $axios.$put(url + '/question', {
      _id, 
      data
    })
  },

  // deleteQuestion({kode_soal}) {
  //   return $axios.$delete(url + '/question', {
  //     data: { id: kode_soal }
  //   })
  // },

  deleteQuestion({_id}) {
    return $axios.$delete(url + '/question', {
      data: { _id }
    })
  },

  getDomain({type}) {
    return $axios.$get( url + '/domain', {
      params: {
        type
      }
    });
  }

})

export default ({
  $axios
}, inject) => {
  inject('question', model($axios));
}