export default function ({
  $axios,
  $auth,
  store,
  redirect
}) {
  $axios.onRequest(config => {
    config.headers.common['Accept'] = 'application/json';
    config.headers.common['Content-Type'] = 'application/json';
    // config.headers.common["Access-Control-Allow-Origin"] = '*';
    // config.headers.common["Access-Control-Allow-Headers"] = 'Origin, X-Requested-With, Content-Type, Accept';
    console.log('Making request to ', config.url);
  });
}
