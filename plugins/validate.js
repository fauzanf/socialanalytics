import { ValidationProvider } from 'vee-validate';
import Vue from 'vue'

Vue.component('ValidationProvider', ValidationProvider);