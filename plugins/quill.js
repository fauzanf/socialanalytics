import Vue from 'vue'
import {
  VueEditor, Quill
} from 'vue2-editor';
import { ImageDrop } from 'quill-image-drop-module'
import ImageResize from 'quill-image-resize'
Quill.register('modules/imageDrop', ImageDrop)
Quill.register('modules/imageResize', ImageResize)

Vue.use(VueEditor);
Vue.component('vue-editor', VueEditor);