import firebase from 'firebase/app'
import 'firebase/database'
import 'firebase/firestore'

if (!firebase.apps.length) {
    const config = {
        apiKey: "AIzaSyByEVYrjnnQNdspH_qDBBQc53K4fjL2e1o",
        authDomain: "cermat-jarak-jauh.firebaseapp.com",
        databaseURL: "https://cermat-jarak-jauh.firebaseio.com",
        projectId: "cermat-jarak-jauh",
        storageBucket: "cermat-jarak-jauh.appspot.com",
        messagingSenderId: "126553074757",
        appId: "1:126553074757:web:07481daefe6b7b44e4872c",
        measurementId: "G-1SMPQLH1Z7"
    }
    firebase.initializeApp(config)
}

export const fireDb = firebase.database()
export const fireStore = firebase.firestore()