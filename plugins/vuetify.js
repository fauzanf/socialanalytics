import Vue from 'vue'
import Vuetify from 'vuetify/lib'
import colors from 'vuetify/es5/util/colors'

Vue.use(Vuetify, {
  theme: {
    primary: '#0100cc',
    primary2: '#0100cc',
    accent: '#444444',
    secondary: "#012147",
    // colors.amber.darken3,
    info: colors.teal.lighten1,
    warning: colors.amber.base,
    error: colors.deepOrange.accent4,
    success: colors.green.accent3,
    primary_light: '#e8f7ff',
    blue_text: '#0166FF',
    ig_color: '#FD1D1D',
    twitter_color: '#1DA1F2',
    fb_color: '#1877F2'
  }
})
