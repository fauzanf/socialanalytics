import Vue from 'vue';

Vue.mixin({
  methods: {
    $checkRoles: (roles, allowed) => {
      return roles.some(role => allowed.includes(role));
    }
  },

  data: () => ({
    roles: {
      isAll: [
        'pusat',
        'provinsi',
        'kokab',
        'sekolah',
        'guru',
        'murid',
        999
      ],
      isAdminPusat: ['pusat'],
      isAdminProvinsi: ['provinsi'],
      isAdminKokab: ['kokab'],
      isAdminSekolah: ['sekolah'],
      isGuru: ['guru'],
      isMurid: ['murid'],
      isSuperAdmin: ['superadmin'],
      isSuper: [999]
    },
  })
})